# Fuzzing for Javascript Example

This is an example of how to integrate your [jsfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/jsfuzz/-/tree/master) targets into GitLab CI/CD

This example will show the following steps:
* [Building and running a simple javafuzz target locally](#building-and-running-a-jsfuzz-target)
* [Running the javafuzz target via GitLab CI/CD](#running-jsfuzz-fuzz-from-ci)

Result:
* jsfuzz targets will run a test on the master branch on every commit.
* jsfuzz targets will run regression tests on every merge request (and every other branch) with the generated corpus and crashes to catch bugs early on.

Fuzzing for Javascript can help find both complex bugs and correctness bugs. Java is a safe language so memory corruption bugs
are unlikely to happen, but bugs can still have security implications like DoS attacks. See [Trophies](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/jsfuzz/-/tree/master#trophies)

This tutorial focuses less on how to build jsfuzz targets and more on how to integrate the targets with GitLab. A lot of 
great information is available at the [jsfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/jsfuzz) repository.

## Building and running a jsfuzz target

The example is located in the [jsfuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/jsfuzz) repository.

## Running jsfuzz from CI

The best way to integrate jsfuzz fuzzing with Gitlab CI/CD is by adding additional stage & step to your `.gitlab-ci.yml`.

```yaml

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml

my_fuzz_target:
  extends: .fuzz_base
  script:
    - npm config set @gitlab-org:registry https://gitlab.com/api/v4/packages/npm/ && npm i -g @gitlab-org/jsfuzz
    - ./gitlab-cov-fuzz run --regression=$REGRESSION --engine jsfuzz -- fuzz.js
```

For each fuzz target you will will have to create a step which extends `.fuzz_base` that runs the following:
* Builds the fuzz target
* Runs the fuzz target via `gitlav-cov-fuzz` CLI.
* Choose the `jsfuzz` fuzz engine with `--engine javafuzz`
* For `$CI_DEFAULT_BRANCH` (can be override by `$COV_FUZZING_BRANCH`) will run fully fledged fuzzing sessions.
  For everything else including MRs will run fuzzing regression with the accumlated corpus and fixed crashes.  

